#ifndef __VERSION_H
#define __VERSION_H

signed int get_version_major(void);
signed int get_version_minor(void);
signed int get_version_patch(void);
const char* get_version_string(void);

#endif //__VERSION_H