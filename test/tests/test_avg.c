#include "avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* set stuff up here */
}

void tearDown(void)
{
    /* clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(10, integer_avg(5, 15), "Error in integer_avg, avg(5,10) should be 10");
    TEST_ASSERT_EQUAL_INT_MESSAGE(25, integer_avg(20, 30), "Error in integer_avg, avg(20,30) should be 25");
}

